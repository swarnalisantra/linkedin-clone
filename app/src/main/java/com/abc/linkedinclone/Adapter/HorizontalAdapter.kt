package com.abc.linkedinclone.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.abc.linkedinclone.DataItem.HorizontalItem
import com.abc.linkedinclone.R
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.horizontal_recyclerview_listitem.view.*

class HorizontalAdapter( val context: Context,private val statusList:List<HorizontalItem>): RecyclerView.Adapter<HorizontalAdapter.Viewholder>() {
    class Viewholder(itemView: View): RecyclerView.ViewHolder(itemView),View.OnClickListener {
        val profileImage: CircleImageView = itemView.image
        var profileName: TextView = itemView.name
        override fun onClick(itemView: View?) {
            itemView?.setOnClickListener(this)
           // Toast.makeText(this,"Status",Toast.LENGTH_LONG).show()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.horizontal_recyclerview_listitem,parent,false)
        return Viewholder(itemView)
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
       val currentItem = statusList[position]
        holder.profileImage.setImageResource(currentItem.image)
        holder.profileName.text = currentItem.name
    }

    override fun getItemCount(): Int {
        return statusList.size
    }


}