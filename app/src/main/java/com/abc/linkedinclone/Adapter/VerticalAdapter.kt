package com.abc.linkedinclone.Adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.abc.linkedinclone.DataItem.HorizontalItem
import com.abc.linkedinclone.DataItem.VerticalItem
import com.abc.linkedinclone.R
import com.bumptech.glide.Glide
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.horizontal_recyclerview_listitem.view.*
import kotlinx.android.synthetic.main.vertical_recyclerview_listitem.view.*

class VerticalAdapter( val context: Context,private val statusList:List<VerticalItem>): RecyclerView.Adapter<VerticalAdapter.Viewholder>() {
    class Viewholder(itemView: View): RecyclerView.ViewHolder(itemView),View.OnClickListener {
        val profileImage: CircleImageView = itemView.image_profile
        var profileName: TextView = itemView.tvName
        val profileDesignation:TextView = itemView.tvDesignation
        val profileTime:TextView = itemView.tvTime
        val postStatus:TextView = itemView.tvStatus
        val postImage:ImageView = itemView.postImage
        val postLike:TextView = itemView.tvLike
        val postComment:TextView = itemView.tvComment
        override fun onClick(itemView: View?) {
            itemView?.setOnClickListener(this)
            // Toast.makeText(this,"Status",Toast.LENGTH_LONG).show()
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Viewholder {
        var itemView = LayoutInflater.from(parent.context).inflate(R.layout.vertical_recyclerview_listitem,parent,false)
        return Viewholder(itemView)
    }

    override fun onBindViewHolder(holder: Viewholder, position: Int) {
        val currentItem = statusList[position]
        holder.profileImage.setImageResource(currentItem.proPic)
        holder.profileName.text = currentItem.name
        holder.profileDesignation.text = currentItem.designation
        holder.profileTime.text = currentItem.time
        holder.postStatus.text = currentItem.status
        holder.postImage.setImageResource(currentItem.postPic)
        holder.postLike.text = currentItem.likes.toString()
        holder.postComment.text = currentItem.comments.toString()
    }

    override fun getItemCount(): Int {
        return statusList.size
    }


}