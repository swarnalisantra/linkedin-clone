package com.abc.linkedinclone.DataItem

data class VerticalItem(val id:Int,val likes:Int,val comments:Int,val proPic:Int,val postPic:Int,
val name:String,val designation:String,val time:String,val status:String)
