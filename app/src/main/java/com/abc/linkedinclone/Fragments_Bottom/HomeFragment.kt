package com.abc.linkedinclone.Fragments_Bottom

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.abc.linkedinclone.DataItem.HorizontalItem
import com.abc.linkedinclone.Adapter.HorizontalAdapter
import com.abc.linkedinclone.Adapter.VerticalAdapter
import com.abc.linkedinclone.DataItem.VerticalItem
import com.abc.linkedinclone.R
import kotlin.collections.ArrayList

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
private const val ARG_PARAM1 = "param1"
private const val ARG_PARAM2 = "param2"

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class HomeFragment : Fragment() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {

        }

    }

    private fun generateDummyList(size: Int): List<HorizontalItem> {
        val list = ArrayList<HorizontalItem>()
        for (i in 0 until size) {
            val drawable = when (i % 3) {
                0 -> R.drawable.profile
                1 -> R.drawable.profile
                else -> R.drawable.profile
            }
            val item = HorizontalItem(drawable, "Swarnali Santra")
            list += item
        }
        return list
    }
   

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        initHorizontalRecyclerView(view)
        initVerticalRecyclerView(view)
        return view

    }

    private fun initHorizontalRecyclerView(view: View) {
        val rv = view.findViewById<RecyclerView>(R.id.horizontalRview)
        rv.adapter = context?.let { HorizontalAdapter(it, generateDummyList(100)) }
        rv.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        rv.setHasFixedSize(true)
    }

    private fun initVerticalRecyclerView(view: View) {
        val rv = view.findViewById<RecyclerView>(R.id.verticalRview)
        rv.adapter = context?.let { VerticalAdapter(it, generateDummyList2(100)) }
        rv.layoutManager = LinearLayoutManager(context)
        rv.setHasFixedSize(true)
    }

    private fun generateDummyList2(size: Int): List<VerticalItem> {
        val list = ArrayList<VerticalItem>()
        for (i in 0 until size) {
            val proPic = when (i % 3) {
                0 -> R.drawable.profile
                1 -> R.drawable.profile
                else -> R.drawable.profile
            }
                val postPic = when (i % 3) {
                    0 -> R.drawable.post
                    1 -> R.drawable.post
                    else -> R.drawable.post
                }
                val item = VerticalItem(
                    1,
                    18,
                    5,
                    proPic,
                    postPic,
                    "Swarnali Santra",
                    "Developer at CodelogicX",
                    "11 hours",
                    "This is dummy status"
                )
                list += item
            }
            return list
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment HomeFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance(param1: String, param2: String) =
            HomeFragment().apply {
                arguments = Bundle().apply {

                }
            }
    }
}